export interface ILoginItems {
    email: string,
    password: string,
    fullName?: string,
    confirmationPassword?: string
}
export type TValidateLoginRegisterFunction = (values: ILoginItems) => {
    email: string,
    password: string,
    fullName?: string,
    confirmationPassword?: string
}
