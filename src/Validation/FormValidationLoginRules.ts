import {ILoginItems} from "../Models/Models";

const ValidateLoginFunction = (values: ILoginItems) => {
    let errorMessages: ILoginItems = {
        email: '',
        password: '',
    };
    if (!values.email) {
        errorMessages.email = 'Email address is required';
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
        errorMessages.email = 'Invalid e-mail address';
    }
    if (!values.password) {
        errorMessages.password = 'Password is required';
    } else if (values.password.length < 8) {
        errorMessages.password = 'Password must be 8 or more characters';
    }
    return errorMessages;
};

export default ValidateLoginFunction;
