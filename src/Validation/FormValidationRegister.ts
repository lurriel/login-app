import {ILoginItems} from '../Models/Models';
import ValidateLoginFunction from './FormValidationLoginRules';

const ValidateRegisterFunction = (values: ILoginItems) => {
    let errorMessages: ILoginItems = {
        email: '',
        password: '',
        fullName: '',
        confirmationPassword: ''
    };
    if (!values.confirmationPassword) {
        errorMessages.confirmationPassword = 'Confirmation password is required';
    } else if (values.confirmationPassword.length < 8) {
        errorMessages.confirmationPassword = 'Password must be 8 or more characters';
    }
    if (!values.fullName) {
        errorMessages.fullName = 'Full name is required';
    } else if (!/^[a-zA-Z]{2,} [a-zA-Z]{2,}/.test(values.fullName)) {
        errorMessages.fullName = 'Missing last name';
    }
    errorMessages = Object.assign(errorMessages,ValidateLoginFunction(values))
    if(values.confirmationPassword && values.confirmationPassword.length > 0 && values.password !== values.confirmationPassword) {
        errorMessages.confirmationPassword = 'Password does not match'
    }
    return errorMessages;
};

export default ValidateRegisterFunction;
