import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Login from './Login';

Enzyme.configure({adapter: new Adapter()});

describe('Login test', () => {
  const wrapper = shallow(<Login/>);
  it('Should contain login class', () => {
    const className = wrapper.find('.login');
    expect(className).toHaveLength(1);
  });

  it('Should contain form', () => {
    const formObject = wrapper.find('form');
    expect(formObject).toHaveLength(1);
  });

  it('Should contain green button', () => {
    const className = wrapper.find('.button-green');
    expect(className).toHaveLength(1);
  });
});
