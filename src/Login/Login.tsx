// eslint-disable-next-line
import React from 'react';
import SwitchButton from '../SwitchButton/SwitchButton';
import ValidateLoginFunction from '../Validation/FormValidationLoginRules';
import UseForm from '../UseForm/UseForm';

const Login = () => {
  const {
    values,
    errors,
    actionChange,
    actionSubmit,
  } = UseForm('login', ValidateLoginFunction);

  return (
    <div >
      <form onSubmit={actionSubmit} noValidate>
        <h1>Login</h1>
        <div className={'login'}>
          <div>E-mail address</div>
          <div className='input-field'>
            <input
              className={errors.email ? 'input-error' : ''}
              type="email"
              name="email"
              placeholder='Email Address'
              onChange={actionChange}
              value={values.email || ''}
              required
              autoComplete="off"
            />
            {errors.email && (
              <p className="field-wrong">{errors.email}</p>
            )}
          </div>
          <div>Password</div>
          <div className='input-field'>
            <input
              className={errors.password ? 'input-error' : ''}
              name='password'
              type='password'
              placeholder='Password'
              onChange={actionChange}
              value={values.password || ''}
              required
            />
            {errors.password && (
              <p className="field-wrong">{errors.password}</p>
            )}
          </div>
          <button className='button-green' type='submit'>Login</button>
        </div>
        <SwitchButton login={true} />
      </form>
    </div>
  );
};

export default Login;
