import React, {useState, useEffect} from 'react';
import {TValidateLoginRegisterFunction, ILoginItems} from '../Models/Models';

const UseForm = (formType: string,
                 validate: TValidateLoginRegisterFunction) => {
  let formItems: ILoginItems;
  if (formType === 'Login') {
    formItems = {email: '', password: ''};
  } else {
    formItems = {email: '',
      password: '',
      fullName: '',
      confirmationPassword: ''};
  }

  const [values, setValues] = useState(formItems);
  const [errors, setErrors] = useState(formItems);
  const [submitInProgress, setSubmitInProgress] = useState(false);

  useEffect(() => {
    if (!Object.keys(errors).some((error: string) => errors[error]) && submitInProgress) {
      // Future function call/redirection after successful Login/Register
      window.alert('Welcome!')
      console.log('Welcome!');
    }
  }, [errors, submitInProgress]);

  const actionSubmit = (event: React.FormEvent<EventTarget>) => {
    if (event) event.preventDefault();
    setErrors(validate(values));
    setSubmitInProgress(true);
  };

  const actionChange = (event: any) => {
    event.persist();
    setValues((values) => (
      {...values, [event.target.name]: event.target.value}
    ));
  };

  return {
    actionChange,
    actionSubmit,
    values,
    errors,
  };
};

export default UseForm;
