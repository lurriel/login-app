import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Register from './Register';

Enzyme.configure({adapter: new Adapter()});

describe('Register test', () => {
  const wrapper = shallow(<Register/>);
  it('Should contain register class', () => {
    const className = wrapper.find('.register');
    expect(className).toHaveLength(1);
  });

  it('Should contain form', () => {
    const formObject = wrapper.find('form');
    expect(formObject).toHaveLength(1);
  });

  it('Should contain H1 with Register string', () => {
    const hOne = wrapper.find('h1');
    expect(hOne.text()).toEqual('Register');
  });
});
