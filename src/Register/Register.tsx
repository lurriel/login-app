// eslint-disable-next-line
import React from 'react';
import SwitchButton from '../SwitchButton/SwitchButton';
import UseForm from '../UseForm/UseForm';
import ValidateRegisterFunction from '../Validation/FormValidationRegister';

const Register = () => {
  const {
    values,
    errors,
    actionChange,
    actionSubmit,
  } = UseForm('Register', ValidateRegisterFunction);

  return (
    <div>
      <form onSubmit={actionSubmit} noValidate>
        <h1>Register</h1>
        <div className='register'>
          <div>E-mail address</div>
          <div className='input-field'>
            <input
              className={errors.email ? 'input-error' : ''}
              type="email"
              name="email"
              placeholder='Email Address'
              onChange={actionChange}
              value={values.email || ''}
              required
              autoComplete="off"
            />
            {errors.email && (
              <p className="field-wrong">{errors.email}</p>
            )}
          </div>
          <div>Full name</div>
          <div className='input-field'>
            <input
              className={errors.fullName ? 'input-error' : ''}
              name='fullName'
              placeholder='Name'
              value={values.fullName || ''}
              onChange={actionChange}
              required
              autoComplete="off"
            />
            {errors.fullName && (
              <p className="field-wrong">{errors.fullName}</p>
            )}
          </div>
          <div>Password</div>
          <div className='input-field'>
            <input
              className={errors.password ? 'input-error' : ''}
              name='password'
              type='password'
              placeholder='Password'
              onChange={actionChange}
              value={values.password || ''}
              required
            />
            {errors.password && (
              <p className="field-wrong">{errors.password}</p>
            )}
          </div>
          <div>Confirm your password</div>
          <div className='input-field'>
            <input
              className={errors.confirmationPassword ? 'input-error' : ''}
              type='password'
              name='confirmationPassword'
              placeholder='Password'
              onChange={actionChange}
              value={values.confirmationPassword || ''}
              required
            />
            {errors.confirmationPassword && (
              <p className="field-wrong">{errors.confirmationPassword}</p>
            )}
          </div>
          <button className='button-green' type='submit'>Register</button>
        </div>
        <SwitchButton login={false}/>
      </form>
    </div>
  );
};

export default Register;
