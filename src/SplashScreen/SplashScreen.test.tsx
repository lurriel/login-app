import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SplashScreen from './SplashScreen';

Enzyme.configure({adapter: new Adapter()});

describe('SplashScreen test', () => {
  const wrapper = shallow(<SplashScreen/>);
  it('Should contain splash-screen class', () => {
    const className = wrapper.find('.splash-screen');
    expect(className).toHaveLength(1);
  });

  it('Should contain user-panel class', () => {
    const className = wrapper.find('.user-panel');
    expect(className).toHaveLength(1);
  });

  it('Should contain two Route modules', () => {
    const moduleName = wrapper.find('Route');
    expect(moduleName).toHaveLength(2);
  });
});
