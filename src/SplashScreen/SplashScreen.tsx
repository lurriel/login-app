// eslint-disable-next-line
import React from 'react';
import Login from '../Login/Login';
import Register from '../Register/Register';
import {BrowserRouter as Router, Route} from "react-router-dom";

const SplashScreen = () => {
  return (
    <div className='splash-screen' >
      <div className='user-panel'>
          <div className='image-header'/>
        <Router>
          <Route  path="/" exact component={() => <Login/>} />
          <Route  path="/register" exact component={() => <Register/>} />
        </Router>
      </div>
    </div>
  );
};

export default SplashScreen;
