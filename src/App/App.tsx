// eslint-disable-next-line
import React from 'react';
import '../Style/App.scss';
import SplashScreen from '../SplashScreen/SplashScreen';

const App = () => {
  return (
    <div className="App">
        <SplashScreen/>
    </div>
  );
}

export default App;
