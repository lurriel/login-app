import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';

Enzyme.configure({adapter: new Adapter()});

describe('App test', () => {
  it('Should contain App class name', () => {
    const wrapper = shallow(<App/>);
    const appName = wrapper.find('.App');
    expect(appName).toHaveLength(1);
  });
});
