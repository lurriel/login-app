import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SwitchButton from './SwitchButton';

Enzyme.configure({adapter: new Adapter()});

describe('SwitchButton test', () => {
  const wrapper = shallow(<SwitchButton/>);
  it('Should contain login class', () => {
    const className = wrapper.find('.click-on-account');
    expect(className).toHaveLength(1);
  });

  it('Should contain text', () => {
    const className = wrapper.find('.click-on-account');
    expect(className.text()).toEqual('Click here to sign in');
  });
});
