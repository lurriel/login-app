// eslint-disable-next-line
import React from 'react';
import {Link} from "react-router-dom";

const SwitchButton = (props: any ) => {
  let intro: string = '';
  let linkText: string = 'Click here to ';
  let linkAddress: string = '';
  if (props.login) {
    intro = 'Not our member yet?';
    linkText += 'create new account';
    linkAddress = '/register';
  } else {
    intro = 'Already using our app?';
    linkText += 'sign in';
    linkAddress = '/'
  }

  return (
    <div className='account-box' >
      <p>{intro}</p>
      <Link className='click-on-account' to={linkAddress}>
        {linkText}
      </Link>
    </div>
  );
};

export default SwitchButton;
